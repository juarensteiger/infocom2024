using JuMP, HiGHS, Statistics, Distributed, LinearAlgebra, Plots, Plots.PlotMeasures, LaTeXStrings, Distributions


# v[n,m] is utility of dispatcher n sending to server m
# z[n,m] indicates dispatcher n can send to server m
# la[n] is arrival mean of dispatcher n
# mu[m] is service mean of server m
# al[m] is server cost of server m 
# be[n] is cost budget of dispatcher n
# returns model, decision variables r, server slack variables y, and dispatcher slack variables s
function fluid_linprog(v, z, la, mu, al, be, silent=true)
    @assert(size(v) == size(z))
    N, M = size(v)
    @assert(length(la) == N)
    @assert(length(mu) == M)

    model = Model(HiGHS.Optimizer)
    if silent set_silent(model) end

    @variable(model, 0 <= r[n=1:N, m=1:M])
    @variable(model, 0 <= y[m=1:M]) # server slack variables
    @variable(model, 0 <= s[n=1:N]) # dispatcher slack variables

    
    for n=1:N @constraint(model, sum(r[n,m] for m=1:M) + s[n] == la[n]) end
    for m=1:M @constraint(model, sum(r[n,m] for n=1:N) + y[m] == mu[m]) end

    for n=1:N, m=1:M @constraint(model, r[n,m] <= z[n,m]*la[n]) end 
    for n=1:N @constraint(model, sum(al[m]*r[n,m] for m=1:M) - be[n] <= 0) end

    @objective(model, Max, sum(sum(v[n,m]*r[n,m] for m=1:M) for n=1:N))
    optimize!(model)
    return (model, value.(r), value.(y), value.(s))
end

mutable struct TreeNode
    isserver::Bool
    index::Int
    parent::TreeNode
    children::Vector{TreeNode}
    TreeNode() = new()
end

# helper function for JSQ_K that generates spanning forest for a network
# r and y come from fluid_linprog
function get_spanning_forest(r, y, N, M)
    @assert(size(r) == (N,M))
    @assert(length(y) == M)
    @assert(all(x -> 0 <= x, r))
    @assert(all(x -> 0 <= x, y))

    # mark idle dispatchers and servers as visited
    dispatcher_visited = [sum(r[n,m] for m=1:M) == 0 for n=1:N]
    server_visited = [sum(r[n,m] for n=1:N) == 0 for m=1:M]

    function expand_treenode!(treenode)
        if treenode.isserver # children are dispatchers
            # find unvisited dispatchers with essential links to server treenode
            children_idxs = findall([r[n, treenode.index] > 0 && !dispatcher_visited[n] for n=1:N])
        else # children are servers
            # find unvisited servers with essential links to dispatcher treenode
            children_idxs = findall([r[treenode.index, m] > 0 && !server_visited[m] for m=1:M])
        end
        for child_idx in children_idxs
            child = TreeNode(); child.parent = treenode; child.index = child_idx # create and init child node
            child.isserver = !treenode.isserver; child.children = [] # create and init child node
            push!(treenode.children, child) # add child to children of treenode
            if treenode.isserver # child is a dispatcher
                dispatcher_visited[child_idx] = true # mark visited
            else # child is a server
                server_visited[child_idx] = true # mark visited
            end
        end
        for child in treenode.children # recursively expand children
            expand_treenode!(child)
        end
    end

    function find_root_index()
        unvisited_slack_idx = findfirst([y[m] > 0 && !server_visited[m] for m=1:M]) # find unvisited slack server 
        if unvisited_slack_idx === nothing # no unvisited slack servers
            return findfirst([!server_visited[m] for m=1:M]) # return any unvisited server
        else
            return unvisited_slack_idx
        end
    end

    forest = TreeNode[]
    # find unvisited slack server to set as root
    root_idx = find_root_index()
    while root_idx !== nothing 
        root = TreeNode(); root.index = root_idx; root.isserver = true; root.children = []
        server_visited[root_idx] = true # mark root server as visited
        expand_treenode!(root)
        push!(forest, root)
        root_idx = find_root_index()
    end

    return forest
end

mutable struct NodeConnections
    primaries::Vector{Int}
    secondary::Int
end

# helper function for JSQ_K
function get_node_connections(forest, N, M)
    server_dispatchers = [NodeConnections([], 0) for m=1:M]
    dispatcher_servers = [NodeConnections([], 0) for n=1:N]

    function traverse_tree(treenode)
        if isdefined(treenode, :parent)
            if treenode.isserver
                server_dispatchers[treenode.index].secondary = treenode.parent.index
            else
                dispatcher_servers[treenode.index].secondary = treenode.parent.index
            end
        end
        for child in treenode.children
            if treenode.isserver
                push!(server_dispatchers[treenode.index].primaries, child.index)
            else
                push!(dispatcher_servers[treenode.index].primaries, child.index)
            end
            traverse_tree(child)
        end
    end

    for tree in forest
        traverse_tree(tree)
    end

    return (server_dispatchers, dispatcher_servers)
end


function get_random_uniform(means)
    return means + rand([-2,-1,0,1,2], size(means))
end

function get_random_gaussian(means)
    return means + rand(Normal(0,1), size(means))
end

function get_random_bernoulli(means)
    if length(size(means)) == 2
        return [rand(Bernoulli(means[i,j])) for i=1:size(means)[1],  j=1:size(means)[2]]
    elseif length(size(means)) == 1
        return [rand(Bernoulli(means[i])) for i=1:size(means)[1]]
    else
        @assert(1 == 2) # don't care about supporting higher dimensions
    end
end

# index based routing policy that routes jobs from each dispatcher n to a server m with the highest index[n,m]
# Q[m] is the job queue for server m 
# arrivals[n] is the number of job arrivals at dispatcher n
# returns number of jobs dropped for each dispatcher, number of arrivals to each server, and the routing decision
function index_based_routing!(Q, arrivals, indexes, N, M)
    jobs_dropped = [0 for n=1:N]; server_arrivals = [0 for m=1:M]; route = zeros(N,M)
    for n=1:N
        idx, m = findmax(indexes[n, :])
        if idx > 0 # then send all jobs from n to m
            route[n,m] = arrivals[n]
            server_arrivals[m] += arrivals[n]
            for job=1:arrivals[n]
                pushfirst!(Q[m], n)
            end
        else # else drop all jobs from n
            jobs_dropped[n] += arrivals[n]
        end
    end
    return jobs_dropped, server_arrivals, route
end

# static routing policy derived from fluid linear program solution
# all variables are the same as previous functions
function static_routing!(Q, arrivals, r, s, la, N, M)
    jobs_dropped = [0 for n=1:N]; server_arrivals = [0 for m=1:M]; route = zeros(N,M)
    for n=1:N
        for job=1:arrivals[n]
            if rand(Bernoulli(max(s[n]/la[n],0)))
                jobs_dropped[n] += 1 
            else
                m = findfirst(z->z==1, rand(Multinomial(1, [max(r[n,m]/la[n],0) for m=1:M])))
                pushfirst!(Q[m], n)
                server_arrivals[m] += 1
                route[n,m] += 1
            end
        end
    end
    return jobs_dropped, server_arrivals, route
end

# priority-based routing policy implemented according to Fu and Modiano
# Q_h and Q_l are high and low priority virtual queues
# all other variables are the same as previous functions
function JSQ_K_routing!(Q_h, Q_l, arrivals, dispatcher_servers, N, M, K)
    jobs_dropped = [0 for n=1:N];  route = zeros(N,M)
    for n=1:N
        if length(dispatcher_servers[n].primaries) > 0
            m_star = 1
            for m in dispatcher_servers[n].primaries # find shortest low priority queue
                if length(Q_l[m]) < length(Q_l[m_star]) 
                    m_star = m 
                end
            end
            if length(Q_l[m_star]) <= K # if shortest low priority queue of primary servers is shorter than K
                route[n, m_star] += arrivals[n]
                for job=1:arrivals[n] # route all jobs to shortest low priority queue of primary servers
                    pushfirst!(Q_l[m_star], n) 
                end
                continue # continue to next dispatcher
            end
        end
        # if high priority queue of secondary server is shorter than K
        if dispatcher_servers[n].secondary != 0 && length(Q_h[dispatcher_servers[n].secondary]) <= K
            route[n, dispatcher_servers[n].secondary] += arrivals[n]
            for job=1:arrivals[n] # route all jobs to high priority queue of secondary server
                pushfirst!(Q_h[dispatcher_servers[n].secondary], n) 
            end
            continue # continue to next dispatcher
        end
        jobs_dropped[n] += arrivals[n] # if we did not route any jobs from dispatcher n
    end
    return jobs_dropped, route
end

# simple first-in-first out scheduling policy
# all variables are the same as previous functions
# returns number of dispatcher-n jobs completed for each server m
function FIFO_scheduling!(Q, offered_service, N, M)
    remaining_service = copy(offered_service)
    jobs_completed = [0 for n=1:N,m=1:M]
    for m=1:M
        while remaining_service[m] > 0
            if length(Q[m]) > 0
                n = pop!(Q[m])
                jobs_completed[n,m] += 1
            else # no more jobs in queue!
                break
            end
            remaining_service[m] -= 1
        end
    end
    return jobs_completed
end

# priority-based scheduling policy implemented according to Fu and Modiano
# all variables are the same as previous functions
function JSQ_K_scheduling!(Q_h, Q_l, offered_service, N, M)
    remaining_service = copy(offered_service)
    jobs_completed = [0 for n=1:N,m=1:M]
    for m=1:M
        while remaining_service[m] > 0
            if length(Q_h[m]) > 0 
                n = pop!(Q_h[m])
                jobs_completed[n,m] += 1
            elseif length(Q_l[m]) > 0 
                n = pop!(Q_l[m])
                jobs_completed[n,m] += 1
            else # no more jobs in queues!
                break
            end
            remaining_service[m] -= 1
        end
    end
    return jobs_completed
end

# joint routing and scheduling policy implemented according to Fu and Modaino
# returns utility over time, queue length over time, and additional constraint functions g over time
function JSQ_K(v, z, la, mu, al, be, K, dispatcher_servers; T=200000+1, bernoulli_setting=false)
    N, M = size(v)
    utility_record = zeros(T); queue_record = zeros(T); g_record = zeros(N,T)
    Q_h = [[] for m=1:M]; Q_l = [[] for m=1:M]
    for t=1:T
        arrivals = bernoulli_setting ? get_random_bernoulli(la) : get_random_uniform(la)
        offered_service = bernoulli_setting ? get_random_bernoulli(mu) : get_random_uniform(mu)

        jobs_completed = JSQ_K_scheduling!(Q_h, Q_l, offered_service, N, M)
        jobs_dropped, route = JSQ_K_routing!(Q_h, Q_l, arrivals, dispatcher_servers, N, M, K)
        
        g_t = [sum(al[m]*route[n,m] for m=1:M) - be[n] for n=1:N]

        if bernoulli_setting
            utilities = [sum(get_random_bernoulli(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        else
            utilities = [sum(get_random_gaussian(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        end
        utility_record[t] = sum(utilities) 
        queue_record[t] = sum(length(Q_h[m]) + length(Q_l[m]) for m=1:M)
        for n=1:N
            g_record[n, t] = g_t[n]
        end
    end
    return utility_record, queue_record, g_record
end

# static policy with static routing according to fluid LP and FIFO scheduling
# all variables are as in previous functions
function static(v, z, la, mu, al, be, r, s; T=200000+1, bernoulli_setting=false)
    N, M = size(v)
    utility_record = zeros(T); queue_record = zeros(T); g_record = zeros(N,T)
    Q = [[] for m=1:M]
    for t=1:T
        arrivals = bernoulli_setting ? get_random_bernoulli(la) : get_random_uniform(la)
        offered_service = bernoulli_setting ? get_random_bernoulli(mu) : get_random_uniform(mu)

        jobs_dropped, server_arrivals, route = static_routing!(Q, arrivals, r, s, la, N, M)

        jobs_completed = FIFO_scheduling!(Q, offered_service, N, M)

        g_t = [sum(al[m]*route[n,m] for m=1:M) - be[n] for n=1:N]
        
        if bernoulli_setting
            utilities = [sum(get_random_bernoulli(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        else
            utilities = [sum(get_random_gaussian(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        end
        
        utility_record[t] = sum(utilities) 
        queue_record[t] = sum(length(Q[m]) for m=1:M)
        for n=1:N
            g_record[n, t] = g_t[n]
        end
    end
    return utility_record, queue_record, g_record
end

# our proposed POLAR algorithm, or the POND algorithm if the POND flag is set to true
# bernoulli_setting flag toggles between bernoulli and gaussian rewards
# all other variables are as in previous functions
function POLAR(v, z, la, mu, al, be, K; T=200000+1, bernoulli_setting=false, POND=false)
    N, M = size(v)
    utility_record = zeros(T); queue_record = zeros(T); g_record = zeros(N,T)
    Q = [[] for m=1:M]
    Q_virt  = zeros(M); Z_virt = zeros(N)
    h = zeros(N,M); v_est = zeros(N,M)
    ucb = [(bernoulli_setting && !POND) ? 1. : 1e6 for n=1:N, m=1:M]
    
    for t=1:T
        arrivals = bernoulli_setting ? get_random_bernoulli(la) : get_random_uniform(la)
        offered_service = bernoulli_setting ? get_random_bernoulli(mu) : get_random_uniform(mu)

        ucb_tune = POND ? 2*sqrt(T) : K*sqrt(t)
        indexes = [z[n,m] == 1 ? (ucb_tune*ucb[n,m] - Q_virt[m] - al[m]*Z_virt[n]) : -1 for n=1:N,m=1:M]
        jobs_dropped, server_arrivals, route = index_based_routing!(Q, arrivals, indexes, N, M)

        jobs_completed = FIFO_scheduling!(Q, offered_service, N, M)

        epsilon_t = POND ? 0.5/sqrt(T) : 0.5*(t/(t+1) + log(t+1))/sqrt(t*log(t+1))
        for m=1:M
            Q_virt[m] = max(Q_virt[m] + server_arrivals[m] - offered_service[m] + epsilon_t, 0)
        end
        g_t = [sum(al[m]*route[n,m] for m=1:M) - be[n] for n=1:N]
        for n=1:N
            Z_virt[n] = max(Z_virt[n] + g_t[n] + epsilon_t, 0)
        end

        
        if bernoulli_setting
            utilities = [sum(get_random_bernoulli(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        else
            utilities = [sum(get_random_gaussian(fill(v[n,m], jobs_completed[n,m]))) for n=1:N,m=1:M]
        end
        for n=1:N, m=1:M
            v_est[n,m] = (h[n,m]*v_est[n,m] + utilities[n,m]) / max(h[n,m] + jobs_completed[n,m], 1)
            h[n,m] += jobs_completed[n,m]
            if h[n,m] > 0
                if POND
                    ucb[n,m] =  v_est[n,m] + sqrt(log(T)/h[n,m])
                elseif bernoulli_setting
                    # display(v_est[n,m])
                    # display(h[n,m])
                    ucb[n,m] = min(1., v_est[n,m] + sqrt(1.5*log(t)/h[n,m]))
                else
                    ucb[n,m] =  v_est[n,m] + sqrt(2*log(1+t*(log(t)^2))/h[n,m])
                end
            end
        end

        utility_record[t] = sum(utilities) 
        queue_record[t] = sum(length(Q[m]) for m=1:M)
        for n=1:N
            g_record[n, t] = g_t[n]
        end
    end
    return utility_record, queue_record, g_record
end

function plot_results(results, num_sims, T;  
    num_points=25, xlabel=L"round $t$", ylabel=L"result at round $t$", legend=:topleft, backend=pgfplotsx,
    labels=[L"test1" L"test2"], markers=[:+ :star], colors=[:green :blue], linestyles=[:dash :dot], ymax=-Inf)
    @assert(length(results) == length(labels) == length(markers) == length(colors) == length(linestyles))
    @assert(all(z -> size(z) == (num_sims, T), results))
    means = reduce(hcat, [transpose(mean(results[i], dims=1)) for i=1:length(results)])
    errors = reduce(hcat, [(1.96/sqrt(num_sims))*transpose(std(results[i], dims=1)) for i=1:length(results)])
    step = trunc(Int, T/num_points)
    backend()
    return plot(collect(1:T)[1:step:end], means[1:step:end, :], ribbon=errors[1:step:end, :], fillalpha=.1,
        xlabel=L"$\mathrm{timeslot}\,\, t\times 10^5$", legend=legend, xlims=(0,T+100), ylims=(0,ymax), left_margin=0mm, right_margin=1mm,
        label=labels, marker=markers, color=colors, linestyle=linestyles, legendfontsize=13, thickness_scaling=.7, 
        markersize=6, size=(350,275), tickfont=11, xaxis=(formatter=x->string(x/10^5)), xguidefontsize=16, yrotation=60, 
        top_margin=-1mm, bottom_margin=2mm)
end


# generate params for Fu and Modiano setting

N_fu, M_fu = (5,4)
la_fu = [2., 4., 4., 5., 4.]
mu_fu = [7., 7., 7., 7.]

v_fu = zeros(5,4)
v_fu[1,1] = 5.
v_fu[2,1] = 5.; v_fu[2,2] = 2.
v_fu[3,1] = 3.; v_fu[3,3] = 3; v_fu[3,4] = 2.
v_fu[4,2] = 2.; v_fu[4,3] = 4.
v_fu[5,3] = 2.; v_fu[5,4] = 4.

z_fu = [(v_fu[n,m] > 0.) for n=1:N_fu ,m=1:M_fu]

al_fu = zeros(M_fu); be_fu = zeros(N_fu)

model_fu, r_fu, y_fu, s_fu = fluid_linprog(v_fu, z_fu, la_fu, mu_fu, al_fu, be_fu)

# check that solution matches Fig. 3.a in Fu and Modiano
@assert(r_fu[1,1] == 2)
@assert(r_fu[2,1] == 4)
@assert(r_fu[3,1] == 1)
@assert(r_fu[3,3] == 2)
@assert(r_fu[3,4] == 1)
@assert(r_fu[4,3] == 5)
@assert(r_fu[5,4] == 4)

genie_util_fu = objective_value(model_fu)

num_sims = 500
T=200000+1

# run simulations for Fu and Modiano setting

jsq_regret_fu = zeros(num_sims, T); jsq_queue_fu = zeros(num_sims, T)
K = 20*log(T)
forest_fu = get_spanning_forest(r_fu, y_fu, N_fu, M_fu)
server_dispatchers_fu, dispatcher_servers_fu = get_node_connections(forest_fu, N_fu, M_fu)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = JSQ_K(v_fu, z_fu, la_fu, mu_fu, al_fu, be_fu, K, dispatcher_servers_fu; T=T, bernoulli_setting=false)
        jsq_regret_fu[sim, :] =  cumsum(genie_util_fu .- utility_record)
        jsq_queue_fu[sim, :] = queue_record
    end
end

polar_regret_fu = zeros(num_sims, T); polar_queue_fu = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = POLAR(v_fu, z_fu, la_fu, mu_fu, al_fu, be_fu, K; T=T, bernoulli_setting=false, POND=false)
        polar_regret_fu[sim, :] =  cumsum(genie_util_fu .- utility_record)
        polar_queue_fu[sim, :] = queue_record
    end
end

pond_regret_fu = zeros(num_sims, T); pond_queue_fu = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = POLAR(v_fu, z_fu, la_fu, mu_fu, al_fu, be_fu, K; T=T, bernoulli_setting=false, POND=true)
        pond_regret_fu[sim, :] =  cumsum(genie_util_fu .- utility_record)
        pond_queue_fu[sim, :] = queue_record
    end
end

static_regret_fu = zeros(num_sims, T); static_queue_fu = zeros(num_sims, T)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = static(v_fu, z_fu, la_fu, mu_fu, al_fu, be_fu, r_fu, s_fu; T=T, bernoulli_setting=false)
        static_regret_fu[sim, :] =  cumsum(genie_util_fu .- utility_record)
        static_queue_fu[sim, :] = queue_record
    end
end

plt = plot_results([polar_regret_fu, pond_regret_fu, jsq_regret_fu, static_regret_fu], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Cumulative Regret",
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot],
    backend=gr)
savefig(plt, "regret_fu.pdf")


plt = plot_results([polar_queue_fu, pond_queue_fu, jsq_queue_fu, static_queue_fu], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Total Queue Lengths",
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot],
    backend=gr)
savefig(plt, "queue_fu.pdf")


# generate params for setting from Fig. 1 without additional constraints

N_st, M_st = (2,3)

la_st = [.5, .5]
mu_st = [.5, .5, .2]

v_st = zeros(N_st, M_st)
v_st[1,1] = .5; v_st[1,2] = .3; v_st[1,3] = .7
v_st[2,1] = .3; v_st[2,2] = .5; v_st[2,3] = .7

z_st = [(v_st[n,m] > 0.) for n=1:N_st ,m=1:M_st]

al_st = zeros(M_st); be_st = zeros(N_st)

model_st, r_st, y_st, s_st = fluid_linprog(v_st, z_st, la_st, mu_st, al_st, be_st)

genie_util_st = objective_value(model_st)


# run simulations for setting from Fig. 1 without additional constraints

jsq_regret_st = zeros(num_sims, T); jsq_queue_st = zeros(num_sims, T)
K = 20*log(T)
forest_st = get_spanning_forest(r_st, y_st, N_st, M_st)
server_dispatchers_st, dispatcher_servers_st = get_node_connections(forest_st, N_st, M_st)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = JSQ_K(v_st, z_st, la_st, mu_st, al_st, be_st, K, dispatcher_servers_st; T=T, bernoulli_setting=true)
        jsq_regret_st[sim, :] =  cumsum(genie_util_st .- utility_record)
        jsq_queue_st[sim, :] = queue_record
    end
end

polar_regret_st = zeros(num_sims, T); polar_queue_st = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = POLAR(v_st, z_st, la_st, mu_st, al_st, be_st, K; T=T, bernoulli_setting=true, POND=false)
        polar_regret_st[sim, :] =  cumsum(genie_util_st .- utility_record)
        polar_queue_st[sim, :] = queue_record
    end
end


pond_regret_st = zeros(num_sims, T); pond_queue_st = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = POLAR(v_st, z_st, la_st, mu_st, al_st, be_st, K; T=T, bernoulli_setting=true, POND=true)
        pond_regret_st[sim, :] =  cumsum(genie_util_st .- utility_record)
        pond_queue_st[sim, :] = queue_record
    end
end

static_regret_st = zeros(num_sims, T); static_queue_st = zeros(num_sims, T)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, queue_record, _ = static(v_st, z_st, la_st, mu_st, al_st, be_st, r_st, s_st; T=T, bernoulli_setting=true)
        static_regret_st[sim, :] =  cumsum(genie_util_st .- utility_record)
        static_queue_st[sim, :] = queue_record
    end
end


plt = plot_results([polar_regret_st, pond_regret_st, jsq_regret_st, static_regret_st], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Cumulative Regret", ymax=1500,
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot],
    backend=gr)
savefig(plt, "regret_st.pdf")


plt = plot_results([polar_queue_st, pond_queue_st, jsq_queue_st, static_queue_st], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Total Queue Lengths",
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot],
    backend=gr)
savefig(plt, "queue_st.pdf")


# generate params for setting from Fig. 1 with server cost constraints

al_stc = [.5, .5, .9]; be_stc = [.3, .3]

model_stc, r_stc, y_stc, s_stc = fluid_linprog(v_st, z_st, la_st, mu_st, al_stc, be_stc)

genie_util_stc = objective_value(model_stc)


# run simulations for setting from Fig. 1 with server cost constraints

jsq_regret_stc = zeros(num_sims, T); jsq_peak_stc = zeros(num_sims, T)
K = 20*log(T)
forest_stc = get_spanning_forest(r_stc, y_stc, N_st, M_st)
server_dispatchers_stc, dispatcher_servers_stc = get_node_connections(forest_stc, N_st, M_st)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, _, g_record = JSQ_K(v_st, z_st, la_st, mu_st, al_stc, be_stc, K, dispatcher_servers_stc; T=T, bernoulli_setting=true)
        jsq_regret_stc[sim, :] =  cumsum(genie_util_st .- utility_record)
        jsq_peak_stc[sim, :] = sum(accumulate(max, cumsum(g_record, dims=2), dims=2), dims=1)
    end
end


polar_regret_stc = zeros(num_sims, T); polar_peak_stc = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, _, g_record = POLAR(v_st, z_st, la_st, mu_st, al_stc, be_stc, K; T=T, bernoulli_setting=true, POND=false)
        polar_regret_stc[sim, :] =  cumsum(genie_util_stc .- utility_record)
        polar_peak_stc[sim, :] = sum(accumulate(max, cumsum(g_record, dims=2), dims=2), dims=1)
    end
end


pond_regret_stc = zeros(num_sims, T); pond_peak_stc = zeros(num_sims, T)
K=2
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, _, g_record = POLAR(v_st, z_st, la_st, mu_st, al_stc, be_stc, K; T=T, bernoulli_setting=true, POND=true)
        pond_regret_stc[sim, :] =  cumsum(genie_util_stc .- utility_record)
        pond_peak_stc[sim, :] = sum(accumulate(max, cumsum(g_record, dims=2), dims=2), dims=1)
    end
end


static_regret_stc = zeros(num_sims, T); static_peak_stc = zeros(num_sims, T)
@time begin
    @sync @distributed for sim = 1:num_sims
        utility_record, _, g_record = static(v_st, z_st, la_st, mu_st, al_stc, be_stc, r_stc, s_stc; T=T, bernoulli_setting=true)
        static_regret_stc[sim, :] =  cumsum(genie_util_stc .- utility_record)
        static_peak_stc[sim, :] = sum(accumulate(max, cumsum(g_record, dims=2), dims=2), dims=1)
    end
end

plt = plot_results([polar_regret_stc, pond_regret_stc, jsq_regret_stc, static_regret_stc], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Cumulative Regret", ymax=3000,
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot], legend=:topright,
    backend=gr)
savefig(plt, "regret_stc.pdf")


plt = plot_results([polar_peak_stc, pond_peak_stc, jsq_peak_stc, static_peak_stc], num_sims, T, 
    labels=reshape(["POLAR", "POND", "JSQ-K", "Static"], (1,4)),
    ylabel="Total Queue Lengths", ymax=200,
    markers=[:+ :x :rtriangle :ltriangle],  
    colors=[:blue :green :red :orange], linestyles=[:dash :solid :dashdot :dot], legend=:topright,
    backend=gr)
savefig(plt, "peak_stc.pdf")