# IEEE INFOCOM 2024 Code

Code for our paper *Backlogged Bandits: Cost-Effective Learning for Utility Maximization in Queueing Networks* tested on Julia version 1.8. 

Run infocom2024.jl after installing the required packages in the `using` statement from Julia's builtin package manager Pkg.

The script saves the plots as PDF files in the folder containing it. 
